# API Gateway

<img src="zuul.png">

 - O `API Gateway`cria um único ponto de acesso à nossa aplicação.
 - Uma `API Gateway` é útil para que os usuários consigam acessar as funcionalidades implementadas em vários microsserviços, sem que os usuários (aplicativo móvel, web, etc) tenham a inteligência de saber como encontrar os microsserviços.
 - Não é recomendado expor o server `Eureka` na Internet para que os front-ends descubram as instâncias disponíveis.
 - O `Zuul` utiliza o `Eureka` para conhecer as instâncias dos microsserviços e, usando o Ribbon, fazer o balanceamento de carga das requisições dos usuários
 - Documentação:
 	- [Spring Router and Filter](https://cloud.spring.io/spring-cloud-netflix/multi/multi__router_and_filter_zuul.html)
 - Habilita repasse de Headers na comunicação com outras instâncias de microservices:
 ```
 zuul:
  sensitive-headers: 
  - Cookie, Authorization
 ```
 
##### Request

 - GET:
 	- http://localhost:5555/actuator/routes
 
 - Acessando outro Microservice através do API Gateway (`api_gateway_ip_porta + nome_microservico_eureka + path`). Exemplo:
 	- GET: http://localhost:5555/fornecedor/info/DF
 	
##### Repassando Header (token) para microservices

 - Configuração que habilita repasse de Headers (Cookies e Authorization[token]) na comunicação com outras instâncias de microservices

```     
zuul:
  sensitive-headers: 
  - Cookie, Authorization
```

